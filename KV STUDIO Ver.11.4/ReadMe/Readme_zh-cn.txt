========================================================================
=                                                             2021/11  =
=                       KV STUDIO Ver.11.4                             =
=                                                                      =
=                       基恩士(中国)有限公司                           =
=                                                                      =
=                     https://china.keyence.com/                       =
=                                                                      =
========================================================================


KV STUDIO 是一款操作简单，处理功能强大的梯形图编程软件。
该软件可以实现高效编程，有效利用已有梯形图程序资产。

■KV STUDIO Ver.11.3 ⇒ Ver.11.4的追加功能
  追加OPC UA服务器功能
  追加ST/脚本的多次粘贴功能
  在变量编辑窗口的右击菜单中追加“插入”

■KV STUDIO Ver.11.1 ⇒ Ver.11.3的追加功能
  支持功能块数组类型的变量
  在指令后缀追加.A
　功能块、功能属性追加“自变量值传递时自动转换类型“设定
  ST/KV脚本等多行注释输出功能
  将运转记录保存的触发后收集时间上限从60秒扩展至86400秒
　实时时序图监控的节拍比较功能
　强调关系映射分析结果的功能
　支持字符串类型数组变量
  支持数据活用单元（KV-XD02）
  追加KV-CA02的连接机型(KV-CAC1H/KV-CAC1R)

■KV STUDIO Ver.11.0 ⇒ Ver.11.1的追加功能
　支持CPU单元（KV-8000A）
  支持ST语言
  支持KV-XLE02的PLC链接设定的变量
  追加KV-XLE02的PLC链接设定的机种
  追加每个KV传感器网络机器的结构体设定
  支持变量/引数的要素注释设定
  软元件注释的最大字符数扩展到128个字符

■KV STUDIO Ver.10.0 ⇒ Ver.11.0的追加功能
  KV-8000系列CPU功能版本2.0公开
  梯形图监控数据生成
  支持变量
  支持结构体
  支持在梯形图程序指定数组元素
  数组・结构体指令(WSIZE,BSIZE,ACNT,BBMOV,BBCMP)
  功能块指令(FBCALL,FBSTRT)
  支持功能块的数据型指定
  支持功能块的EN/ENO指定
  支持功能块的返回值设定
  支持在搜索/替换中用正规表现进行搜索

■KV STUDIO Ver.9.4 ⇒ Ver.10.0的追加功能
  支持CPU单元（KV-8000）
  支持相机输入单元（KV-CA02）
  重放模式
  运转记录设定
  软元件变化点查找
  关系映射
  重放VT浏览器联动
  运转记录指令
  事件/错误监控器
  日志/跟踪设定的拷贝＆粘贴功能
  实时时序图监控器的波形比较功能
  KV-8000, KV-XLE02的系统功能块
  KV-XLE02/XL202/XL402的PROTOCOL STUDIO功能的通信指令密码设定

■KV STUDIO Ver.9.3 ⇒ Ver.9.4的追加功能
  支持EtherNet/IP通信单元（KV-EP02）
  支持EtherNet/IP设定的连接设备自动设定(自动配置)
  EtherNet/IP设定的适配器设定传输中适配器读取/写入按钮放入工具内
  更改「工具」「帮助」下的菜单构成

■KV STUDIO Ver.9.2 ⇒ Ver.9.3的追加功能
  KV-7000系列的R软元件点数扩展(从16000点扩展至32000点)
  KV STUDIO显示语言中追加「繁体中文」和「韩文」
  KV-7000系列的项目语言设定中追加「繁体中文」和「韩文」
  KV-7000系列的软元件注释的注释集语言中追加「繁体中文」和「韩文」
  改善KV-XLE02/XL202/XL402的PLC链路设定的拷贝&粘帖功能
  KV-XLE02/XL202/XL402的PLC链路设定的多次粘帖功能
  改善KV脚本的RT编辑候选显示
  软元件注释编辑窗口中追加带软元件编号的拷贝
  改善KV-XLE02/XL202/XL402的通信测试功能的记录显示内容。

■KV STUDIO Ver.9.1 ⇒ Ver.9.2的追加功能
  支持Ethernet单元(KV-XLE02)
  支持串行通信单元(KV-XL202/XL402)
  强化快捷键自定义功能
  PLC程序读出时的软元件值批量读出功能
  实时时序图监控器的RT编辑功能
  KV-XH16ML/XH04ML的监控模式切换时的传输确认消息功能

■KV STUDIO Ver.9.0 => Ver.9.1的追加功能
  实时时序图监控器的XY显示
  实时时序图监控器的组设定
  KV-XH16ML/XH04ML程序包
  KV-XH16ML/XH04ML系统功能块
  KV-XH16ML/XH04ML增加凸轮设定区间数
  KV-XH16ML/XH04ML凸轮曲线中追加3次曲线，5次曲线
  KV-XH16ML/XH04ML伺服报警监控器中追加报警跟踪功能
  支持功能块/功能不指定单元变量的种类
  单元种类判断指令
  缓冲存储器/单元内部软元件监控器
  用户功能块

■KV STUDIO Ver.8.1 => Ver.9.0的追加功能
  支持MECHATROLINK-Ⅲ定位・运动单元(KV-XH16ML/XH04ML)
  发布KV-7000系列CPU功能版本2.0
  功能块/功能
  KV-XH16ML/XH04ML系统功能块
  程序执行停止时的OFF处理设定
  ATAN2指令(从坐标计算弧度角度)
  单元程序指令
  APR/MAX/MIN/AVG/WSUM支持浮点型小数
  单元间同步跟踪
  单元间同步CPU软元件值读写功能

■KV STUDIO Ver.8.0 => Ver.8.1 的追加功能
  支持脉冲串定位单元(KV-SH04PL)
  支持高速计数器单元(KV-SSC02)
  支持实时曲线监控在模拟器
  RT 编辑搜索功能改善

■KV STUDIO Ver.7.4 => Ver.8.0 的追加功能
  支持 KV-7000 系列
  支持 Windows 8/8.1
  支持 Unicode字符
  CPU 存储器容量设定
  消息编辑器
  模拟器编辑
  VT模拟器联动
  KV STUDIO 语言设定
  存储体传输工具
  通用注释字库编辑

■KV STUDIO Ver.7.3 => Ver.7.4 的追加功能
  KV-L2*V中追加PROTOCOL STUDIO Lite模式

■KV STUDIO Ver.7.1 => Ver.7.3 的追加功能
  支持通信型定位单元(KV-LH20V)
  KV-LH20V的连接机器中新增QS系列

■KV STUDIO Ver.7.0 => Ver.7.1 的追加功能
  支持 KV Nano 系列(连接器型)

■KV STUDIO Ver.6.1 => Ver.7.0 的追加功能
  支持 KV Nano 系列
  单元编辑器显示改善
  支持串行通讯单元(KV-L21V)
  支持高速多链路单元(KV-LM21V)
  新增”帮助导航（参考程序/技术资料等）“帮助菜单

■KV STUDIO Ver.5.5 => Ver.6.1 的追加功能
  支持 Bluetooth 单元
  新增模拟量单元
  新增 IO 单元
  支持 Windows 7(32/64bit)
  支持 EtherNet/IP单元
  新增数据链路设定软件(KV DATALINK+ for EtherNet/IP)
  传感器监控器
  批量传输传感器设定
  传感器设定备份
  RT 编辑
  微分监控器
  性能监控器
  新增指令(支持双精度, PID控制指令, 存储卡指令)
  传感器专用监视器
  远程XG显示器
  在梯形图显示 RT 编辑操作数输入支持

■KV STUDIO Ver.5 => Ver.5.5 的追加功能
  支持定位/运动单元
  增加参数设置软件 KV MOTION+（设置 KV-ML/MC）

■KV STUDIO Ver.4 => Ver.5 的追加功能
  支持 Windows Vista
  集成了相关软件（MOTION BUILDER、PROTOCOL STUDIO、
  MV LINK STUDIO）

■KV STUDIO Ver.3 => Ver.4 的追加功能
  支持 KV-5000/3000 系列
  单元编辑器功能增强
  并排显示/自动隐藏
  支持 KV 脚本数组
  局部标号
  记录/跟踪
  支持恒定周期模块
  实时曲线监控
  新增指令
  PLC 校验/同步
  局部软元件注释传送范围设定
  打印功能增强
  监视窗口
  增强 CPU 系统设定
  KV-L20V 单元监控器
  扫描时间监控
  强制置位/复位登录/取消
  错误监控器
  高速程序传送
  按模块传送
  邮件设定
  邮件通讯命令编制者
  软元件初始值设定
  文件寄存器设定

■KV STUDIO Ver.2 => Ver.3 的追加功能
  支持 KV-700/KV-10~40/KV-P16 系列
  支持多次启动
  共享 USB 接口
  读取 KZ-A500/350/300 文件
  编辑支持功能增强
  工作区显示改善

■KV STUDIO Ver.1 => Ver.2 的追加功能
  支持 KV 脚本语言
  检索功能增强
  校验功能增强
  编辑支持功能增强

■KV STUDIO 的安装方法
  执行 setup.exe。

■KV STUDIO 的启动方法
  通过“运行”->“程序”-> “KEYENCE KV STUDIO Ver.11G”->
  “KV STUDIO Ver.11G”，即可启动。

■相关软件的启动方法
  通过“运行”->“程序”-> “KEYENCE KV STUDIO Ver.11G”->
  “工具”，即可启动各相关软件。
  也可以通过 KV STUDIO 启动相关软件。

■卸载方法
  打开控制面板内的“程序和功能”，从列表中选择
  “KEYENCE KV STUDIO Ver.11G”，点击“卸载”按钮。

========================================================================
·Windows 是美国 Microsoft 公司的注册商标。
·Pentium 是美国 Intel 公司的注册商标。
·"EditX: Copyright (C) 1999-2002 by EmSoft, k.k."
·"1998-2003 Codejock 软件，保留所有权利。"
·UNLHA32.DLL 为 Micco 先生提供的免费软件。
·VS-FlexGrid Pro Copyright(C) 2000 VideoSoft Corporation
·其它的公司名称及产品名称分别为各公司的注册商标或商标。
                                                                    结束