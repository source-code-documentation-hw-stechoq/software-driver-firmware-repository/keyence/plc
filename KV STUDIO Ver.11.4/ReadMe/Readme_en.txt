========================================================================
=                                                             2021/11  =
=                         KV STUDIO Ver.11.4                           =
=                                                                      =
=                         Keyence Corporation                          =
=                                                                      =
=                      https://www.keyence.com.sg/                     =
=                                                                      =
========================================================================


KV STUDIO is a ladder programming software with high operability and 
powerful processing function, This software is effective for efficient programming 
and can ensure the ladder asset security.

* New features between KV STUDIO Ver.11.3 and Ver.11.4
  OPC UA server function added
  Function for pasting multiple items in ST/Script added
  "Insert" added to right-click menu in variable editing window

* New features between KV STUDIO Ver.11.1 and Ver.11.3
  Function block array type variables supported
  .A added to instruction suffixes
　"Automatically Convert Types When Passing Arguments by Value" setting added to function block and function properties
  Function for commenting out multiple rows in ST/KV script, etc.
  Upper limit on post-trigger collection time for saving operation records increased from 60 to 86400 seconds
　Function for comparing cycles in the real-time chart monitor
　Function for emphasizing analysis results in relation mapping
　String type array variables supported  
  Data utilization unit (KV-XD02) supported
  KV-CA02 connection models (KV-CAC1H/KV-CAC1R) added

* New features between KV STUDIO Ver.11.0 and Ver.11.1
　Supported CPU unit (KV-8000A).
  Supported ST language.
  Supported parameters for PLC link settings of KV-XLE02. 
  Added models to PLC link settings of KV-XLE02.
  Added structure settings by KV sensor network device.
  Supported element comment settings for variables/parameters.
  Extended the max number of characters for device comments to 128. 

* New features between KV STUDIO Ver.10.0 and Ver.11.0
  KV-8000 series CPU function version 2.0
  Ladder monitor data generation
  Variables supported
  Structure supported
  Array type specification supported
  Array / structure command added (WSIZE,BSIZE,ACNT,BBMOV,BBCMP)
  Function block command added (FBCALL,FBSTRT)
  Data type specification for the function block supported
  EN/ENO specification for function block supported
  Return value of the function supported
  Regular expressions in text serch/replacement supported 

* New features between KV STUDIO Ver.9.4 and Ver.10.0
  CPU unit (KV-8000) added
  Camera input unit (KV-CA02) added
  Replay mode
  Operation recorder setting
  Device change point search
  Relation mapping
  Replay VT viewer collaboration
  Operation recorder instruction
  Event /  Error monitor
  Copy & Paste function of Logging / Trace setting
  Waveform comparison function of Real-time Chart Monitor
  Function block of KV-8000 and KV-XLE02
  Password setting of communication command in PROTOCOL STUDIO function of KV-XLE02/XL202/XL402

* New features between KV STUDIO Ver.9.3 and Ver.9.4
  EtherNet/IP supported communication unit (KV-EP02) added
  Auto-configuration of connected devices in EtherNet/IP setting supported
  The buttons in Tool for reading/writing adapter settings of EtherNet/IP setting prepared
  Menu composition of "Tool" and "Help" changed

* New features between KV STUDIO Ver.9.2 and Ver.9.3
  Quantity of relay device for KV-7000 series expanded (16000 to 32000)
  "Chinese(Traditilnal)" and "Korean(Korea)" for KV STUDIO Display language added
  "Chinese (Traditilnal, Taiwan)" and "Korean (Korea)" for project language setting of KV-7000 series added
  "Chinese (Traditilnal, Taiwan)" and "Korean (Korea)" for commentset language of KV-7000 series added
  Copy and paste function of PLC Link setting for KV-XLE02/XL202/XL402 imporoved
  Multi paste function of PLC Link for KV-XLE02/XL202/XL402
  RT Edit candidate display in KV Script improved
  Copy function with device number for device comment edit added
  Display contents of communication test function history for KV-XLE02/XL202/XL402 improved

* New features between KV STUDIO Ver.9.1 and Ver.9.2
  Ethernet Unit (KV-XLE02) supported
  Serial Communication Unit (KV-XL202/XL402) supported
  Shortcut key customization function improved
  Batch readout function of device value when uploads program from PLC
  RT Edit function for Real Time Chart Monitor
  Transfer confirmation message at monitor mode transition of KV-XH16ML/XH04ML

* New features between KV STUDIO Ver.9.0 and Ver.9.1
  XY display for Real time chart monitor
  Group setting for Real time chart monitor
  Application package for KV-XH16ML/XH04ML
  System function block for KV-XH16ML/XH04ML
  Sections of cam setting for KV-XH16ML/XH04ML increased
  Cubic curve and quintic curve of cam for KV-XH16ML/XH04ML added
  Alarm tracing on servo alarm monitor for KV-XH16ML/XH04ML added
  Unit urgument without unit type for function block/function supported
  Unit type judgement instruction
  Monitor for buffer memory and unit internal device
  User function block

* New features between KV STUDIO Ver.8.1 and Ver.9.0
  MECHATROLINK-III positionsing/motion unit (KV-XH16ML/XH04ML) supported
  CPU function version 2.0 for KV-7000 series released
  Function block/function
  System function block for KV-XH16ML/XH04ML
  Setting for OFF process when stopping execution
  ATAN2 instruction for calculating the angle(radians) by coordinate
  Unit program instructions
  Floating type APR/MAX/MIN/AVG/WSUM instructions supported
  Inter-unit synchronization trace
  Reading/writing the value of CPU device for inter-unit synchronization

* New Features between KV STUDIO Ver.8.0 and Ver.8.1
  Pulse train positioning unit (KV-SH04PL) supported
  High-speed counter unit (KV-SSC02) supported
  Real-time chart monitoring in simulator mode supported
  Improved search function of RT Edit

* New Features between KV STUDIO Ver.7.4 and Ver.8.0
  KV-7000 series supported
  Windows 8/8.1 supported
  Unicode supported
  CPU memory capacity settings
  Message editor
  Simulator edit
  VT simulator
  KV STUDIO language setting
  Storage Transfer Tool
  Universal comment dictionary editing

* New Features between KV STUDIO Ver.7.3 and Ver.7.4
  Added PROTOCOL STUDIO Lite mode to KV-L2*V

* New Features between KV STUDIO Ver.7.1 and Ver.7.3
  Communication positioning unit (KV-LH20V) supported
  KV-LH20V connectable device "QS series" added

* New Features between KV STUDIO Ver.7.0 and Ver.7.1
  KV Nano Series (connector type) supported

* New Features between KV STUDIO Ver.6.1 and Ver.7.0
  KV Nano series supported
  Improved Unit Editor display
  Serial Communication unit (KV-L21V) supported
  High-speed multi-link unit (KV-LM21V) supported
  Help menu "Handy-Nav(reference programs/tech materials)" added

* New Features between KV STUDIO Ver.5.5 and Ver.6.1
  Bluetooth unit supported
  Analog unit added
  IO unit added
  Windows 7(32/64) supported
  EtherNet/IP unit supported
  Data link setting software(KV DATALINK+ for EtherNet/IP) added
  Sensor monitor
  Sensor setting batch transmission
  Sensor setting backup
  RT Edit
  Derivation monitor
  Performance monitor
  Instruction added (double pricision supported, PID control, Memory card instruction)
  Special sensor monitor
  Remote XG display
  RT Edit operand candidate display in ladder

* New Features between KV STUDIO Ver.5 and Ver.5.5
  Positioning/motion unit supported
  KV MOTION+ Parametering Software (for KV-ML/MC) added

* New Features between KV STUDIO Ver.4 and Ver.5
  Windows Vista supported
  Related software integrated (such as MOTION BUILDER, PROTOCOL STUDIO, 
  MV LINK STUDIO)

* New Features between KV STUDIO Ver.3 and Ver.4
  KV-5000/3000 series supported
  Enhanced Unit Editor
  Tile/Auto-hide
  KV script set supported
  Local label
  Record/Tracing
  Constant period module supported
  Real-time chart monitoring
  New instructions added
  PLC verify/synchronize
  Local device comments transfer range setting
  Enhanced printing function
  Monitor window
  Enhanced CPU system setting
  KV-L20V Unit Monitor
  Scanning time monitoring
  Forced set/reset register/cancel
  Error monitor
  High-speed program transfer
  Module-wise transfer
  Mail setting
  Mail communication command maker
  Device initial value setting
  File register setting

* New Features between KV STUDIO Ver.2 and Ver.3
  KV-700/KV-10 to 40/KV-P16 series supported
  Multiple-start supported
  Shared USB interface
  Allows to read KZ-A500/350/300 files
  Enhanced edit support function
  Improved workspace display

* New Features between KV STUDIO Ver.1 and Ver.2
  KV script language supported
  Enhanced retrieval function
  Enhanced verification function
  Enhanced edit support function

* How to Install KV STUDIO
  Execute setup.exe.

* How to Start KV STUDIO
  You can start KV STUDIO by "Start" -> "Program" -> "KEYENCE KV STUDIO Ver.11G"
  -> "KV STUDIO Ver.11G".

* How to Start Other Software
  You can start related software by "Start" -> "Program" -> 
  "KEYENCE KV STUDIO Ver.11G" -> "Tools", or by KV STUDIO.

* How to Uninstall
  You can uninstall a software by opening "Programs and Features" in
  control panel and selecting "KEYENCE KV STUDIO Ver.11G" from the list, 
  then clicking "Uninstall" button.

========================================================================
- Windows is a registered trademark of Microsoft Corporation. 
- Pentium is a registered trademark of Intel Corporation. 
- "EditX: Copyright (C) 1999-2002 by EmSoft, k.k."
- "1998-2003 Codejock Software, All Rights Reserved."
- UNLHA32.DLL is a free software provided by Mr. Micco. 
- VS-FlexGrid Pro Copyright(C) 2000 VideoSoft Corporation
- Other company names, product names, and model names used in this manual 
are trademarks or registered trademarks of their respective companies. 
                                                                    END