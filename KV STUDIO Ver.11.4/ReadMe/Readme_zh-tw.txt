========================================================================
=                                                             2021/11  =
=                       KV STUDIO Ver.11.4                             =
=                                                                      =
=                       基恩斯股份有限公司                             =
=                                                                      =
=                    https://www.keyence.com.tw/                       =
=                                                                      =
========================================================================


KV STUDIO 是一款操作簡單，處理功能強大的階梯圖程式設計軟體。
該軟體可以實現高效程式設計，有效利用已有階梯圖程式資產。

■KV STUDIO Ver.11.3 ⇒ Ver.11.4的新增功能
  追加OPC UA伺服器功能
  追加ST/腳本的多次粘貼功能
  在變數編輯視窗的右鍵選單中追加「插入」

■KV STUDIO Ver.11.1 ⇒ Ver.11.3的新增功能
  支援功能塊陣列類型的變數
  於指令尾碼新增.A
　於功能塊、功能的屬性新增「在引數的數值傳送時進行自動類型轉換」設定
  ST/KV腳本等的複數行註釋輸出功能
  將運轉記錄保存的觸發後收集時間的上限從60秒擴展到86400秒
　即時時序圖監控的節拍比較功能
　強調關係對映分析結果的功能
　支援字串類型陣列變數
  支援資料活用單元（KV-XD02）
  新增KV-CA02的連接機種(KV-CAC1H/KV-CAC1R)

■KV STUDIO Ver.11.0 ⇒ Ver.11.1的追加功能
　支援CPU單元（KV-8000A）
  支援ST語言
  支援KV-XLE02的PLC鏈接設定的變數
  追加KV-XLE02的PLC鏈接設定的機種
  追加每個KV感測器網路機器的機構體設定
  支援變數/引數的要素註釋設定
  元件註釋的最大字元數擴展至128個字元

■KV STUDIO Ver.10.0 ⇒ Ver.11.0的追加功能
　KV-8000系列CPU功能版本2.0公開
　梯形圖監控資料生成
　支援變數
　支援結構體
　支援在梯形圖程式指定陣列元素
　陣列・結構體指令(WSIZE,BSIZE,ACNT,BBMOV,BBCMP)
　功能塊指令(FBCALL,FBSTRT)
　支援功能塊的資料型指定
　支援功能塊的EN/ENO指定
　支援功能塊的返回值設定
　支持在搜索/替換中用正規表現進行搜索

■KV STUDIO Ver.9.4 ⇒ Ver.10.0的追加功能
　支援CPU單元（KV-8000）
　支援相機輸入單元（KV-CA02）
　重放模式
　運轉記錄設定
　元件變化點查找
　關係對映
　重放VT瀏覽器聯動
　運轉記錄指令
　事件/錯誤監控器
　記錄/追蹤設定的複製＆貼上功能
　即時時序圖監控器的波形比較功能
　KV-8000, KV-XLE02的系統功能塊
　KV-XLE02/XL202/XL402的PROTOCOL STUDIO功能的通訊指令密碼設定

■KV STUDIO Ver.9.3 ⇒ Ver.9.4的追加功能
  支援EtherNet/IP通訊單元（KV-EP02）
  支援EtherNet/IP設定的連接設備自動設定(自動配置)
  EtherNet/IP設定的適配器設定傳輸中適配器讀取/寫入按鈕放入工具內
  變更「工具」「說明」下的菜單構成

■KV STUDIO Ver.9.2 ⇒ Ver.9.3的追加功能
  KV-7000系列的R元件點數擴充(從16000點擴充至32000點)
  KV STUDIO顯示語言中追加「繁體中文」與「韓文」
  KV-7000系列的專案語言設定中追加「繁體中文」與「韓文」
  KV-7000系列的元件註釋之註釋集語言中追加「繁體中文」與「韓文」
  改善KV-XLE02/XL202/XL402的PLC連接設定之複製&貼上功能
  KV-XLE02/XL202/XL402的PLC連接設定之多次貼上功能
  改善KV腳本的RT編輯候選顯示
  元件註釋編輯窗口中追加帶元件編號的複製
  改善KV-XLE02/XL202/XL402的通訊測試功能之記錄顯示內容

■KV STUDIO Ver.9.1 ⇒ Ver.9.2的追加功能
  支援Ethernet單元(KV-XLE02)
  支援序列通訊單元(KV-XL202/XL402)
  強化快速鍵自訂功能
  PLC程式讀出時的元件值批量讀出功能
  即時時序圖監控器的RT編輯功能
  KV-XH16ML/XH04ML的監控模式切換時之傳輸確認消息功能

■KV STUDIO Ver.9.0 => Ver.9.1的追加功能
  即時時序圖監控器的XY顯示
  即時時序圖監控器的組設定
  KV-XH16ML/XH04ML套裝程式
  KV-XH16ML/XH04ML系統功能塊
  KV-XH16ML/XH04ML增加凸輪設定區間數
  KV-XH16ML/XH04ML凸輪曲線中追加3次曲線，5次曲線
  KV-XH16ML/XH04ML伺服警報監控器中追加警報追蹤功能
  支援功能塊/功能不指定單元變數的種類
  單元種類判斷指令
  緩衝記憶體/單元內部元件監控器
  用戶功能塊

■KV STUDIO Ver.8.1 => Ver.9.0的追加功能
  支援MECHATROLINK-Ⅲ定位・運動單元(KV-XH16ML/XH04ML)
  發佈KV-7000系列CPU功能版本2.0
  功能塊/功能
  KV-XH16ML/XH04ML系統功能塊
  程式執行停止時的OFF處理設定
  ATAN2指令(從座標計算弧度角度)
  單元程式指令
  APR/MAX/MIN/AVG/WSUM支援浮點型小數
  單元間同步追蹤
  單元間同步CPU元件值讀寫功能

■KV STUDIO Ver.8.0 => Ver.8.1 的追加功能
  支援脈波列定位單元(KV-SH04PL)
  支援高速計數器單元(KV-SSC02)
  即時時序圖監控支援模擬器模式
  RT 編輯搜索功能改善

■KV STUDIO Ver.7.4 => Ver.8.0 的追加功能
  支援 KV-7000 系列
  支援 Windows 8/8.1
  支援 Unicode
  CPU 記憶體容量設定
  消息編輯器
  模擬器編輯
  VT模擬器聯動
  KV STUDIO 語言設定
  存儲體傳輸工具
  通用注釋詞典編輯

■KV STUDIO Ver.7.3 => Ver.7.4 的追加功能
  KV-L2*V中追加PROTOCOL STUDIO Lite模式

■KV STUDIO Ver.7.1 => Ver.7.3 的追加功能
  支援通訊型定位單元(KV-LH20V)
  KV-LH20V的連接機器中新增QS系列

■KV STUDIO Ver.7.0 => Ver.7.1 的追加功能
  支援 KV Nano 系列(連接器型)

■KV STUDIO Ver.6.1 => Ver.7.0 的追加功能
  支援 KV Nano 系列
  單元編輯器顯示改善
  支援序列通訊單元(KV-L21V)
  支援高速多鏈路單元(KV-LM21V)
  新增”説明導航（範例程式/技術資料檔等）“説明功能表

■KV STUDIO Ver.5.5 => Ver.6.1 的追加功能
  支援 Bluetooth 單元
  新增類比單元
  新增 IO 單元
  支援 Windows 7(32/64bit)
  支援 EtherNet/IP單元
  新增資料連結設定軟體(KV DATALINK+ for EtherNet/IP)
  感測器監控器
  批量傳輸感測器設定
  感測器設定備份
  RT 編輯
  微分監控器
  性能監控器
  新增指令(支援雙精度, PID控制指令, 記憶卡指令)
  感測器專用監視器
  遠程XG顯示器
  在階梯圖顯示 RT 編輯運算元輸入支援

■KV STUDIO Ver.5 => Ver.5.5 的追加功能
  支援定位/運動單元
  增加參數設置軟體 KV MOTION+（設置 KV-ML/MC）

■KV STUDIO Ver.4 => Ver.5 的追加功能
  支援 Windows Vista
  集成了相關軟體（MOTION BUILDER、PROTOCOL STUDIO、
  MV LINK STUDIO）

■KV STUDIO Ver.3 => Ver.4 的追加功能
  支援 KV-5000/3000 系列
  單元編輯器功能增強
  並列顯示/自動隱藏
  支援 KV 腳本矩陣
  局部標號
  記錄/追蹤
  支援恒定週期模組
  即時時序圖監控
  新增指令
  PLC 校驗/同步
  局部元件注釋傳輸範圍設定
  列印功能增強
  監控視窗
  增強 CPU 系統設定
  KV-L20V 單元監控器
  掃描時間監控
  強制置位/復位登錄/取消
  錯誤監控器
  高速程式傳輸
  按模組傳輸
  郵件設定
  郵件通訊指令編制
  元件初始值設定
  檔寄存器設定

■KV STUDIO Ver.2 => Ver.3 的追加功能
  支援 KV-700/KV-10~40/KV-P16 系列
  支援多次啟動
  共用 USB 介面
  讀取 KZ-A500/350/300 檔
  編輯支援功能增強
  工作區顯示改善

■KV STUDIO Ver.1 => Ver.2 的追加功能
  支援 KV 腳本語言
  搜尋功能增強
  校驗功能增強
  編輯支援功能增強

■KV STUDIO 的安裝方法
  執行 setup.exe。

■KV STUDIO 的啟動方法
  透過“運行”->“程式”-> “KEYENCE KV STUDIO Ver.11G”->
  “KV STUDIO Ver.11G”，即可啟動。

■相關軟體的啟動方法
  透過“運行”->“程式”-> “KEYENCE KV STUDIO Ver.11G”->
  “工具”，即可啟動各相關軟體。
  還可透過 KV STUDIO 啟動相關軟體。

■解除安裝方法
  打開控制台內的“應用程式與功能”，從清單中選擇
  “KEYENCE KV STUDIO Ver.11G”，點擊“解除安裝”按鈕。

========================================================================
·Windows 是美國 Microsoft 公司的註冊商標。
·Pentium 是美國 Intel 公司的註冊商標。
·"EditX: Copyright (C) 1999-2002 by EmSoft, k.k."
·"1998-2003 Codejock 軟體，保留所有權利。"
·UNLHA32.DLL 為 Micco 先生提供的免費軟體。
·VS-FlexGrid Pro Copyright(C) 2000 VideoSoft Corporation
·其他的公司名稱及產品名稱分別為各公司的註冊商標或商標。
                                                                    結束
